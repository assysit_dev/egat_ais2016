var dataJsonG;
$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#checkAll').click(function (event) {
        if(this.checked){
            $('.ck').each(function(){
                this.checked = true;
            });
        }else{
            $('.ck').each(function(){
                this.checked = false;
            });
        }
    });
    var sortBy_hidden=$("#sortBy_hidden").val();
    var orderBy_hidden=$("#orderBy_hidden").val();
    var design_trend_B_hidden=$("#design_trend_B_hidden").val();
    $('select[name="sortBy"]').val(sortBy_hidden)
    $('select[name="orderBy"]').val(orderBy_hidden)
    $('select[name="design_trend_B"]').val(design_trend_B_hidden)

    var nowMoment=moment();
    /*
     var nowFr=nowMoment.format("HH:mm");
     $("#timeHis").val(nowFr)
     */
    var nowStartTimeForDisplay=moment().format("HH:00");
    $("#startTimeForDisplay").val(nowStartTimeForDisplay)

    $("#scaleTimeMinuteExpand").val("4");
    $("#scaleTimeSecondExpand").val("10");
    var currentDateTime=nowMoment.format("DD/MM/YYYY");
    $("#endMonthTime").val(currentDateTime);

    var startMonthMoment =moment().subtract(1, 'years');
    var startMonthDateTime=startMonthMoment.format("DD/MM/YYYY");
    $("#startMonthTime").val(startMonthDateTime);

    $("#endTime").val(currentDateTime);
    var startDayMoment =moment().subtract(1, 'months');
    var startDayDateTime=startDayMoment.format("DD/MM/YYYY");
    $("#startTime").val(startDayDateTime);

    $("#endHRTime").val(currentDateTime);
    var startHRMoment =moment().subtract(2, 'days');
    var startHRDateTime=startHRMoment.format("DD/MM/YYYY");
    $("#startHRTime").val(startHRDateTime);

    $("#oneTime").val(currentDateTime);

 /* */
    $('#datepicker_day').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
    });

    $('#datepicker_HR').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
    });
    // $(".datepicker").datepicker({maxDate: '0'});

    $('#datepicker_month').datepicker({
        minViewMode: 1,
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
    });
    $('#datepicker_onetime').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
    });

    $('.clockpicker').clockpicker(
        {
            afterDone: function() {
            console.log("after done");
            },
            afterHide: function() {
                console.log("after hide");
                showGraph(false,'');
            }
        }
       //  {twelvehour: true}
    );
    /*$('#startTimeForDisplay').clockpicker(
        //  {twelvehour: true}
    );
    */


});
function getMmTrend(A){
    var obj={
        A:A
    }
    $.ajax({
        url: "/ajax/mmtrend/get",
        method: "GET",
        data: obj
    }).done(function(data, status, xhr) {

    });
}
function getMmname(ZZ){
    var obj={
        ZZ:ZZ
    }
    $.ajax({
        url: "/ajax/mmname/get",
        method: "GET",
        data: obj
    }).done(function(data, status, xhr) {

    });
}

function postMmTrend(){

    var obj={
        "aoe":"aoe"
    }
    var param={
        "A":"ChatChai",
        "B":"Pimtun"
    }
    $.ajax({
        url: "/ajax/mmtrend/post",
        method: "POST",
        data: param
    }).done(function(data, status, xhr) {

    });
}

function postMmname(){
    var obj={
        aoe:"aoe"
    }
    var param={
        aoe:obj
    }
    $.ajax({
        url: "/ajax/mmname/post",
        method: "POST",
        data: param
    }).done(function(data, status, xhr) {

    });
}
function doPaging(pageNo){
    $("#mmtrend_pageNo").val(pageNo);
    showmmtrend($("#mmtrend_zz").val())
}
function doSwitchCheckBox(obj_type){
    if(obj_type=='unit'){
       var count=0;
        $( 'input[name="mmplant_check"]:checked' ).each(function( key, value ) {
            count++;
        });
        if(count>0)
            $("#mmplant_check_point_compare").prop('checked',false);
    }else if(obj_type=='point_compare'){
        if($("#mmplant_check_point_compare").prop('checked')){
            $( 'input[name="mmplant_check"]' ).each(function( key, value ) {
                $(this).prop('checked',false);
            });
        }
    }
}
function onChangeCheckPointCompare(obj) {
    doSwitchCheckBox('point_compare');
    if(obj.checked) {
        var checkbox_c_inner_list = [];
        var length = document.getElementsByName('checkbox_inner[]').length;
        for (var i = 0; i < length; i++) {
            var cbx = document.getElementsByName('checkbox_inner[]')[i];
            var c_inner = document.getElementsByName('C_inner[]')[i];
            if (cbx.checked) {
                checkbox_c_inner_list.push(c_inner.value);
            }
        }
        if (checkbox_c_inner_list.length == 0) {
            alert("กรุณาเลือก Point อย่างน้อย 1 Point");
            return false;
        }

       // var length = document.getElementsByName('checkbox_inner[]').length;
        //alert(length)
        for (var i = 0; i < length; i++) {
            var cbx = document.getElementsByName('checkbox_inner[]')[i];
            var c_inner = document.getElementsByName('C_inner[]')[i];
            //alert(b_inner.value)
            for (var j = 0; j < checkbox_c_inner_list.length; j++) {
                if(checkbox_c_inner_list[j]==c_inner.value){
                    //alert(c_inner.value)
                    cbx.checked=true;
                }
            }
        }
    }
}
function doCheckPointCompare() {
    var mmplant_check_point_compare = document.getElementById("mmplant_check_point_compare");
    onChangeCheckPointCompare(mmplant_check_point_compare);
    //onChangeCheckPointCompare($("#mmplant_check_point_compare"));
}
function resetCheckboxInner(){
    var length = document.getElementsByName('checkbox_inner[]').length;
    for (var i = 0; i < length; i++) {
        var cbx = document.getElementsByName('checkbox_inner[]')[i];
        cbx.checked=false;
    }
}
function onChangeCheckMMPLANT(obj){
    doSwitchCheckBox('unit');
    resetCheckboxInner();
    //alert(obj.value+","+obj.checked)
    var length = document.getElementsByName('checkbox_inner[]').length;
    //alert(length)
    /*
    for (var i = 0; i < length; i++) {
        var cbx = document.getElementsByName('checkbox_inner[]')[i];
        var b_inner = document.getElementsByName('B_inner[]')[i];
        if(obj.value==b_inner.value){
            cbx.checked=obj.checked;
        }
    }
    */

    $( 'input[name="mmplant_check"]:checked' ).each(function( key, value ) {
        for (var i = 0; i < length; i++) {
            var cbx = document.getElementsByName('checkbox_inner[]')[i];
            var b_inner = document.getElementsByName('B_inner[]')[i];
            if($(this).val()==b_inner.value){
                cbx.checked= true;//obj.checked;
            }
        }
    });

    doCheckPointCompare();
}
function onChangeMMPLANT() {
    var mmplant_list=[];
    $( 'input[name="mmplant_check"]:checked' ).each(function( key, value ) {
        //alert( key + ": " + value+""+$(this).val() );
        mmplant_list.push($(this).val());
    });
    //alert(mmplant_list)
    var length = document.getElementsByName('checkbox_inner[]').length;
    //alert(length)
    for (var i = 0; i < length; i++) {
        var cbx = document.getElementsByName('checkbox_inner[]')[i];
        var b_inner = document.getElementsByName('B_inner[]')[i];
        //alert(b_inner.value)
        for (var j = 0; j < mmplant_list.length; j++) {
            if(mmplant_list[j]==b_inner.value){
               // alert(b_inner.value)
                cbx.checked=true;
            }


        }
    }
    //showmmtrend($("#mmtrend_zz").val());
}
// for mmtrend list section start
function showmmtrend(zz){
    $("#mmtrend_zz").val(zz)
    $("#trend_element").show();
    var mmtrend_pageNo=$("#mmtrend_pageNo").val();
    var mmtrend_pageSize=$("#mmtrend_pageSize").val();
    var mmplant=$("#mmplant").val();
    var mmplant_list=[];
  //  var mmplant_check=$('input[name="mmplant_check"]:checked').val();{
    $( 'input[name="mmplant_check"]:checked' ).each(function( key, value ) {
        //alert( key + ": " + value+""+$(this).val() );
        mmplant_list.push($(this).val());
        //$(this).attr("checked")();
        $(this).prop('checked',false);
    });
    $("#mmplant_check_point_compare").prop('checked',false);
    //alert(mmplant_check)
    if(mmtrend_pageNo.length==0){
        mmtrend_pageNo="1";
    }
    //alert(mmplant)
    //   mmtrend_pageNo="1";
    // mmtrend_pageSize="20"
    mmplant_list=[];
    var obj={
        zz:zz,
        pageNo:mmtrend_pageNo,
        B:mmplant,
        B_LIST:mmplant_list,
        flag_paging:'0',
        pageSize:mmtrend_pageSize
    }
    $.ajax({
        url: "/ajax/mmtrends/list",
        method: "GET",
        data: obj
    }).done(function(data, status, xhr) {
        console.log(data, status, xhr);
        // alert(data.data.length)
        var trendDesignsM = jQuery.parseJSON(data.trendDesignsM);
        var count = jQuery.parseJSON(data.count);
        var mmnameM = jQuery.parseJSON(data.mmnameM);
        // alert(count)
        if(mmnameM!=null){
            $("#trend_element_header").html("แสดง Point ของ "+mmnameM.A+"  <buton onclick=\"showGraph(false,'')\" class=\"btn btn-primary  btn-sm btnPlotGraph\">Plot Graph </buton>");
        }


        var str=""+
            " <table id=\"editable\" "+
            " class=\"table table-striped table-bordered table-hover  dataTable\" "+
            "  role=\"grid\" aria-describedby=\"editable_info\"> "+
            "  <thead> "+
            "    <tr role=\"row\"> "+
            "    <th class=\"\" tabindex=\"0\" aria-controls=\"\" "+
            " rowspan=\"1\" colspan=\"1\" style=\"width: 0%;\" "+
            " aria-sort=\"\" aria-label=\"\">  <input type='checkbox' id=\"checkAll_inner\"> "+
            "     </th> "+
            "     <th class=\"\" tabindex=\"0\" aria-controls=\"editable\" "+
            " rowspan=\"1\" colspan=\"1\" style=\"width: 10%;\" "+
            " aria-label=\"Browser: activate to sort column ascending\"> "+
            "     Point "+
            "     </th>"+
            "     <th class=\"\" tabindex=\"0\" aria-controls=\"editable\" "+
            " rowspan=\"1\" colspan=\"1\" style=\"width: 5%;\" "+
            " aria-label=\"Platform(s): activate to sort column ascending\"> "+
            "     MM "+
            "    </th> "+
            "    <th class=\"\" tabindex=\"0\" aria-controls=\"editable\" "+
            " rowspan=\"1\" colspan=\"1\" style=\"width: 35%;\" "+
            " aria-label=\"Platform(s): activate to sort column ascending\"> "+
            "     Point Name "+
            " </th> "+
            " <th class=\"\" tabindex=\"0\" aria-controls=\"editable\" "+
            " rowspan=\"1\" colspan=\"1\" style=\"width: 15%;\" "+
            " aria-label=\"Platform(s): activate to sort column ascending\"> "+
            "     Tag Name "+
            "  </th> "+
            " <th class=\"\" tabindex=\"0\" aria-controls=\"editable\" "+
            " rowspan=\"1\" colspan=\"1\" style=\"width: 10%;\" "+
            " aria-label=\"Platform(s): activate to sort column ascending\"> "+
            "     Unit "+
            "     </th> "+
            "     <th class=\"\" tabindex=\"0\" aria-controls=\"editable\" "+
            " rowspan=\"1\" colspan=\"1\" style=\"width: 5%;\" "+
            " aria-label=\"Platform(s): activate to sort column ascending\"> "+
            "     Max "+
            "     </th> "+
            "     <th class=\"\" tabindex=\"0\" aria-controls=\"editable\" "+
            " rowspan=\"1\" colspan=\"1\" style=\"width: 5%;\" "+
            " aria-label=\"Platform(s): activate to sort column ascending\"> "+
            "     Min "+
            "     </th> "+
                /*
            "     <th class=\"\" tabindex=\"0\" aria-controls=\"editable\" rowspan=\"1\" colspan=\"1\"  "+
            "  style=\"width: 10%;\" aria-label=\"Platform(s): activate to sort column ascending\">  "+
            "    Action  "+
            "      </th>  "+
            */
            "    </tr> "+
            "    </thead> "+
            "    <tbody> ";
        /*
         if(trendDesignsM.data!=null && trendDesignsM.data.length>0){
         for(var i=0;i<trendDesignsM.data.length;i++){
         str=str+
         "    <tr class=\"gradeA odd\" role=\"row\"> "+
         "    <td class=\"sorting_1\"> "+
         "    <input type='checkbox' name=\"checkbox_inner[]\" "+
         " class=\"ck_inner\" data-id=\"checkbox\"  value=\""+trendDesignsM.data[i].ZZ+"\"> "+
         "    </td> "+
         "   <td>"+trendDesignsM.data[i].A+"</td> "+
         "   <td>"+trendDesignsM.data[i].B+"</td> "+
         "   <td>"+trendDesignsM.data[i].C+"</td> "+
         " <td>"+trendDesignsM.data[i].D+"</td> "+
         " <td>"+trendDesignsM.data[i].E+"</td> "+
         " <td>"+trendDesignsM.data[i].F0+"</td> "+
         " <td>"+trendDesignsM.data[i].F1+"</td> "+
         " <td> "+
         "  <a id=\"btnEdit\" onclick=\"displayMmtrend('edit','"+trendDesignsM.data[i].ZZ+"','"+trendDesignsM.data[i].H+"')\" class=\"btn btn-dropbox btn-xs\"><i style=\"color: #47a447;\" class=\"glyphicon glyphicon-edit\"></i></a>| "+
         " <a onclick=\"displayMmtrendDelete('delete','"+trendDesignsM.data[i].ZZ+"')\" class=\"btn btn-dropbox btn-xs\"><i class=\"glyphicon glyphicon-trash text-danger\"></i></a> "+
         " </td>" +
         " </tr> ";
         }
         }
         */
        if(trendDesignsM!=null && trendDesignsM.length>0){
            for(var i=0;i<trendDesignsM.length;i++){
                str=str+
                    "    <tr class=\"gradeA odd\" role=\"row\"> "+

                    "    <td class=\"sorting_1\"> "+
                    "    <input type='checkbox' onchange='doCheckPointCompare()' name=\"checkbox_inner[]\" "+
                    " class=\"ck_inner\" data-id=\"checkbox\"  value=\""+trendDesignsM[i].ZZ+"\"> "+
                        "<input type='hidden' name='B_inner[]' value='"+trendDesignsM[i].B+"' />"+
                        "<input type='hidden' name='C_inner[]' value='"+trendDesignsM[i].C+"' />"+
                    "    </td> "+

                    "   <td>"+trendDesignsM[i].A+"</td> "+
                    "   <td>"+trendDesignsM[i].B+"</td> "+
                    "   <td>"+trendDesignsM[i].C+"</td> "+
                    " <td>"+trendDesignsM[i].D+"</td> "+
                    " <td>"+trendDesignsM[i].E+"</td> "+
                    " <td>"+trendDesignsM[i].F0+"</td> "+
                    " <td>"+trendDesignsM[i].F1+"</td> "+
                        /*
                    " <td> "+
                    "  <a id=\"btnEdit\" onclick=\"displayMmtrend('edit','"+trendDesignsM[i].ZZ+"','"+trendDesignsM[i].H+"')\" class=\"btn btn-dropbox btn-xs\"><i style=\"color: #47a447;\" class=\"glyphicon glyphicon-edit\"></i></a>| "+
                    " <a onclick=\"displayMmtrendDelete('delete','"+trendDesignsM[i].ZZ+"')\" class=\"btn btn-dropbox btn-xs\"><i class=\"glyphicon glyphicon-trash text-danger\"></i></a> "+
                    " </td>" +
                    */
                    " </tr> ";
            }
        }
        str=str+" </tbody> "+
            " </table> ";
        var pageStr="<ul class=\"pagination\">";
        var pageNo=parseInt(mmtrend_pageNo);
        var pageSize=parseInt(mmtrend_pageSize);
        var totals=parseInt(count);
        var loopPage=parseInt(totals/pageSize);
        var obsetPage=parseInt(totals%pageSize);
        if(obsetPage>0)
            loopPage++;
        var activePrev="";
        var linkPrev="<a  onclick=\"doPaging('"+(pageNo-1)+"')\" rel=\"prev\">«</a> ";
        if(pageNo==1){
            linkPrev="<span>«</span>";
            activePrev="class=\"disabled\" ";
        }
        if(loopPage>1)
            pageStr=pageStr+" <li "+activePrev+">"+linkPrev+"</li>";
        for(var i=1;i<=loopPage;i++){
            var active="";
            var activeLink="<a onclick=\"doPaging('"+i+"')\" >"+i+"</a>";
            // href=\"http://localhost:8000/ais/statistics?page="+i+"\"
            if(i==pageNo){
                activeLink="<span>"+i+"</span>";
                active=" class=\"active\" ";
            }

            pageStr=pageStr+" <li "+active+">"+activeLink+"</li>";
        }
        var activeNext="";
        var linkNext="<a onclick=\"doPaging('"+(pageNo+1)+"')\" rel=\"next\">»</a> ";
        if(pageNo==loopPage){
            linkNext="<span>»</span>";
            activeNext="class=\"disabled\" ";
        }
        if(loopPage>1)
            pageStr=pageStr+" <li "+activeNext+">"+linkNext+" </li>";
        pageStr=pageStr+"</ul>";
        //alert(loopPage)
        //alert(obsetPage)
        /*
         <div id="paging_element" style="float: right;display: none;">
         <ul class="pagination">
         <li class="disabled"><span>«</span></li>
         <li class="active"><span>1</span>
         </li><li><a href="http://localhost:8000/ais/statistics?page=2">2</a></li>
         <li><a href="http://localhost:8000/ais/statistics?page=3">3</a></li>
         <li><a href="http://localhost:8000/ais/statistics?page=4">4</a></li>
         <li><a href="http://localhost:8000/ais/statistics?page=5">5</a></li>
         <li><a href="http://localhost:8000/ais/statistics?page=6">6</a></li><li>
         <li><a href="http://localhost:8000/ais/statistics?page=2" rel="next">»</a></li>
         </ul>

         </div>
         */
        $("#trend_element_table").html(str);
      //  $("#paging_element").html(pageStr);
        $("#paging_element").show();
        $('#checkAll_inner').click(function (event) {
            if(this.checked){
                $('.ck_inner').each(function(){
                    this.checked = true;
                });
            }else{
                $('.ck_inner').each(function(){
                    this.checked = false;
                });
            }
        });
        //alert(data.trendDesignsM)
        //alert(obj.data[0].C)
        //alert(data.paging)
        //$("#trend_paging").html(data.paging)
    });
}
// start display mmname for edit or save
function searchMmpoint(mmtrend_table_B_selected){
    //alert(mmtrend_table_B_selected)
    var keyword=$("#keyword").val();
    var mode=$("#mmtrend_mode").val();
    var mmtrend_point_zz=$("#mmtrend_point_zz").val();
    var mmtrend_point_h= $("#mmtrend_point_h").val();
    var mmtrend_table_B= $("#mmtrend_table_B").val();
    /*if(mmtrend_table_B_selected=='')
     mmtrend_table_B_selected= $('select[id="mmtrend_table_B"]').val();
     */
    //alert(mode+","+mmtrend_point_h+","+mmtrend_table_B+","+mmtrend_table_B_selected);
    var obj={
        "keyword":keyword,
        "H":mmtrend_point_h,
        "P":mmtrend_table_B
    }
    var tags=['4','5','6','7'];
    var user_mmplant=$("#user_mmplant").val();
    if(user_mmplant!='1'){
        tags=['8','9','10','11','12','13'];
    }
    //alert(id)
    var str=""+
        " <table id=\"editable\" "+
        " class=\"table table-striped table-bordered table-hover  dataTable\" "+
        " role=\"grid\" aria-describedby=\"editable_info\"> "+
        "   <thead> "+
        "   <tr role=\"row\"> "+
        "   <th class=\"\" tabindex=\"0\" aria-controls=\"\" "+
        " rowspan=\"1\" colspan=\"1\" style=\"width: 3%;\" "+
        " aria-sort=\"\" aria-label=\"\"> "+
        "  "+
        "   </th> "+
        "   <th class=\"\" tabindex=\"0\" "+
        " aria-controls=\"editable\" rowspan=\"1\" "+
        " colspan=\"1\" style=\"width: 20%;\" "+
        " aria-label=\"Browser: activate to sort column ascending\"> "+
        "   Point Name "+
        " </th> ";
    for(var i=0;i<tags.length;i++){
        str=str+" <th class=\"\" tabindex=\"0\" "+
            " aria-controls=\"editable\" rowspan=\"1\" "+
            " colspan=\"1\" style=\"width: 13%;\" "+
            " aria-label=\"Platform(s): activate to sort column ascending\"> "+
            "   Tag"+tags[i]+" "+
            "   </th> ";
    }
    /*
     " <th class=\"\" tabindex=\"0\" "+
     " aria-controls=\"editable\" rowspan=\"1\" "+
     " colspan=\"1\" style=\"width: 13%;\" "+
     " aria-label=\"Platform(s): activate to sort column ascending\"> "+
     "   Tag4 "+
     "   </th> "+
     "   <th class=\"\" tabindex=\"0\"  "+
     " aria-controls=\"editable\" rowspan=\"1\" "+
     " colspan=\"1\" style=\"width: 13%;\" "+
     " aria-label=\"Platform(s): activate to sort column ascending\"> "+
     "   Tag5 "+
     "   </th> "+
     "   <th class=\"\" tabindex=\"0\" "+
     " aria-controls=\"editable\" rowspan=\"1\" "+
     " colspan=\"1\" style=\"width: 13%;\" "+
     " aria-label=\"Platform(s): activate to sort column ascending\"> "+
     "   Tag6 "+
     "   </th> "+
     "   <th class=\"\" tabindex=\"0\" "+
     " aria-controls=\"editable\" rowspan=\"1\" "+
     " colspan=\"1\" style=\"width: 13%;\" "+
     " aria-label=\"Platform(s): activate to sort column ascending\"> "+
     "   Tag7 "+
     "   </th> "+
     */
    str=str+"   <th class=\"\" tabindex=\"0\" "+
        " aria-controls=\"editable\" rowspan=\"1\" "+
        " colspan=\"1\" style=\"width: 5%;\" "+
        " aria-label=\"Platform(s): activate to sort column ascending\"> "+
        "   Unit "+
        "   </th> "+
        "   <th class=\"\" tabindex=\"0\" "+
        " aria-controls=\"editable\" rowspan=\"1\" "+
        " colspan=\"1\" style=\"width: 5%;\" "+
        " aria-label=\"Platform(s): activate to sort column ascending\"> "+
        "   Max "+
        "   </th> "+
        "   <th class=\"\" tabindex=\"0\" "+
        " aria-controls=\"editable\" rowspan=\"1\" "+
        " colspan=\"1\" style=\"width: 5%;\" "+
        " aria-label=\"Platform(s): activate to sort column ascending\"> "+
        "   Min "+
        "   </th> "+
        /*
         "   <th class=\"\" tabindex=\"0\" "+
         " aria-controls=\"editable\" rowspan=\"1\" "+
         " colspan=\"1\" style=\"width: 5%;\" "+
         " aria-label=\"Platform(s): activate to sort column ascending\"> "+
         "   Data "+
         "   </th> "+
         */
        "   </tr> "+
        "   </thead> "+
        "   <tbody> ";
    if(mmtrend_table_B=='0' || mmtrend_table_B=='-1' || mmtrend_table_B=='1') {
        str = "" +
            " <table id=\"editable\" " +
            " class=\"table table-striped table-bordered table-hover  dataTable\" " +
            " role=\"grid\" aria-describedby=\"editable_info\"> " +
            "   <thead> " +
            "   <tr role=\"row\"> " +
            "   <th class=\"\" tabindex=\"0\" aria-controls=\"\" " +
            " rowspan=\"1\" colspan=\"1\" style=\"width: 3%;\" " +
            " aria-sort=\"\" aria-label=\"\"> " +
            "  " +
            "   </th> " +
            "   <th class=\"\" tabindex=\"0\" " +
            " aria-controls=\"editable\" rowspan=\"1\" " +
            " colspan=\"1\" style=\"width: 20%;\" " +
            " aria-label=\"Browser: activate to sort column ascending\"> " +
            "   Point Name " +
            " </th> " +
            " <th class=\"\" tabindex=\"0\" " +
            " aria-controls=\"editable\" rowspan=\"1\" " +
            " colspan=\"1\" style=\"width: 13%;\" " +
            " aria-label=\"Platform(s): activate to sort column ascending\"> " +
            "   Tag name " +
            "   </th> " +
            "   <th class=\"\" tabindex=\"0\"  " +
            " aria-controls=\"editable\" rowspan=\"1\" " +
            " colspan=\"1\" style=\"width: 13%;\" " +
            " aria-label=\"Platform(s): activate to sort column ascending\"> " +
            "   MM " +
            "   </th> " +
            "   <th class=\"\" tabindex=\"0\" " +
            " aria-controls=\"editable\" rowspan=\"1\" " +
            " colspan=\"1\" style=\"width: 13%;\" " +
            " aria-label=\"Platform(s): activate to sort column ascending\"> " +
            "   Formula " +
            "   </th> " +

            "   <th class=\"\" tabindex=\"0\" " +
            " aria-controls=\"editable\" rowspan=\"1\" " +
            " colspan=\"1\" style=\"width: 5%;\" " +
            " aria-label=\"Platform(s): activate to sort column ascending\"> " +
            "   Unit " +
            "   </th> " +
            "   <th class=\"\" tabindex=\"0\" " +
            " aria-controls=\"editable\" rowspan=\"1\" " +
            " colspan=\"1\" style=\"width: 5%;\" " +
            " aria-label=\"Platform(s): activate to sort column ascending\"> " +
            "   Max " +
            "   </th> " +
            "   <th class=\"\" tabindex=\"0\" " +
            " aria-controls=\"editable\" rowspan=\"1\" " +
            " colspan=\"1\" style=\"width: 5%;\" " +
            " aria-label=\"Platform(s): activate to sort column ascending\"> " +
            "   Min " +
            "   </th> " +
            /*
             "   <th class=\"\" tabindex=\"0\" "+
             " aria-controls=\"editable\" rowspan=\"1\" "+
             " colspan=\"1\" style=\"width: 5%;\" "+
             " aria-label=\"Platform(s): activate to sort column ascending\"> "+
             "   Data "+
             "   </th> "+
             */
            "   </tr> " +
            "   </thead> " +
            "   <tbody> ";
    }
    $.ajax({
        url: "/ajax/mmpoint/search",
        method: "POST",
        data: obj
    }).done(function(data, status, xhr) {
        console.log(data);
        if(mmtrend_table_B=='0' || mmtrend_table_B=='-1' || mmtrend_table_B=='1'){
            var mmpointM = jQuery.parseJSON(data.mmpointM);
            // alert(mmpointM.length)
            if(mmpointM!=null && mmpointM.length>0) {
                for (var i = 0; i < mmpointM.length; i++) {
                    str = str +
                        " <tr class=\"gradeA odd\" role=\"row\"> "+
                        "  <td class=\"sorting_1\"> ";
                    var checked_str="";
                    if(mmpointM[i].A==mmtrend_point_h){
                        checked_str="checked";
                    }
                    str = str +
                        "  <input type=\"radio\" onclick=\"setMaxMinValue('"+mmpointM[i].F0+"','"+mmpointM[i].F1+"','"+mmpointM[i].E+"')\" name=\"point_ids_input[]\" "+checked_str+" value=\""+mmpointM[i].A+"\"> "+
                        //  " class=\"i-checks\"> "+
                        "     </td> "+
                        "    <td>"+mmpointM[i].C+"</td> "+
                        "    <td>"+mmpointM[i].D+"</td> "+
                        " <td>"+mmpointM[i].B+"</td> "+
                        " <td>"+mmpointM[i].G+"</td> "+
                        " <td>"+mmpointM[i].E+"</td> "+
                        " <td>"+mmpointM[i].F0+"</td> "+
                        " <td>"+mmpointM[i].F1+"</td> "+

                        // " <td>435</td> "+
                        "  </tr> ";
                }
            }
        }else{
            var mmpointM = jQuery.parseJSON(data.mmpointM);
            // alert(mmpointM.length)
            if(mmpointM!=null && mmpointM.length>0) {
                for (var i = 0; i < mmpointM.length; i++) {
                    str = str +
                        " <tr class=\"gradeA odd\" role=\"row\"> "+
                        "  <td class=\"sorting_1\"> ";
                    var checked_str="";
                    if(mmpointM[i].A==mmtrend_point_h){
                        checked_str="checked";
                    }
                    str = str +
                        "  <input type=\"radio\" onclick=\"setMaxMinValue('"+mmpointM[i].G0+"','"+mmpointM[i].G1+"','"+mmpointM[i].F+"')\" name=\"point_ids_input[]\" "+checked_str+" value=\""+mmpointM[i].A+"\"> "+
                        //  " class=\"i-checks\"> "+
                        "     </td> "+
                        "    <td>"+mmpointM[i].B+"</td> ";

                    if(user_mmplant!='1'){
                        str = str +
                            " <td>"+mmpointM[i].C8+"</td> "+
                            " <td>"+mmpointM[i].C9+"</td> "+
                            " <td>"+mmpointM[i].C10+"</td> "+
                            " <td>"+mmpointM[i].C11+"</td> "+
                            " <td>"+mmpointM[i].C12+"</td> "+
                            " <td>"+mmpointM[i].C12+"</td> ";
                    }else{
                        str = str +
                            "  <td>"+mmpointM[i].C4+"</td> "+
                            " <td>"+mmpointM[i].C5+"</td> "+
                            " <td>"+mmpointM[i].C6+"</td> "+
                            " <td>"+mmpointM[i].C7+"</td> ";
                    }

                    str = str +"  <td>"+mmpointM[i].F+"</td> "+
                        " <td>"+mmpointM[i].G0+"</td> "+
                        " <td>"+mmpointM[i].G1+"</td> "+
                        // " <td>435</td> "+
                        "  </tr> ";
                }
            }
        }

        str=str+" </tbody> "+
            " </table> ";

        $("#point_list_section").html(str);
    });

}
function  displayMmtrend(mode,id,h_id){

    $("#mmtrend_mode").val(mode)
    $("#mmtrend_point_zz").val(id)
    $("#mmtrend_point_h").val(h_id)
    //alert($("#mmtrend_point_zz").val())
    var obj={
        "ZZ":id,
        "G":$("#mmtrend_zz").val()

    }
    //alert(id)
    $.ajax({
        url: "/ajax/mmtrend/get",
        method: "GET",
        data: obj
    }).done(function(data, status, xhr) {
        console.log(data);
        var mmtrendM = jQuery.parseJSON(data.mmtrendM);
        var mmnamesM = jQuery.parseJSON(data.mmnameM);
        var mmpointM = jQuery.parseJSON(data.mmpointM);
        $("#mmpoint_table_G0").val('');
        $("#mmpoint_table_G1").val('');
        $("#mmpoint_table_F").val('');

        var mmtrend_tilte="แก้ใข";
        if(mode=='add')
            mmtrend_tilte="เพิ่ม";
        $("#button_mmtrend_mode_section").html(mmtrend_tilte);
        //alert(mmtrendM.F0)
        if(mmtrendM !=null ){
            $("#mmpoint_table_G0").val(mmtrendM.F0);
            $("#mmpoint_table_G1").val(mmtrendM.F1);
            $("#mmpoint_table_F").val(mmtrendM.E);
            $("#mmtrend_group_b").val(mmtrendM.B);
        }
        //  alert(mmnamesM.A)
        $("#mmtrend_tilte_section").html(mmtrend_tilte+" Point ไปที่ "+mmnamesM.A);
        $("#myModalAddPoint").modal()
        $("#keyword").val('');
        searchMmpoint('');
    });
}
//start delete mmtrend
function displayMmtrendDelete(mode,id){
    $("#myModalMmtrendDelete").modal()
    $("#mmtrend_table_mode").val(mode);
    $("#mmtrend_table_zz").val(id);
}
function doDeleteMmtrend(){
    var ids=[];
    var id="";
    var mode = $("#mmtrend_table_mode").val();
    if(mode=='deleteAll') {

        var length = document.getElementsByName('checkbox_inner[]').length;
        for (var i = 0; i < length; i++) {
            var cbx = document.getElementsByName('checkbox_inner[]')[i];
            if (cbx.checked) {
                ids.push(cbx.value)
            }
        }
        if (ids.length == 0) {
            $("#myModalMmtrendDelete").modal('hide')
            return false;
        }
    }else{
        id=$("#mmtrend_table_zz").val();
    }
    var obj={
        "mode":mode,
        "ZZ":id,
        "ids":ids
    }

    $.ajax({
        url: "/ajax/mmtrend/delete",
        method: "DELETE",
        data: obj
    }).done(function(data, status, xhr) {
        //location.reload()
        showmmtrend($("#mmtrend_zz").val());
        $("#myModalMmtrendDelete").modal('hide')
    });
}
// end delete mmtrend

//start delete Mmname
function displayMmnameDelete(mode,id){
    $("#myModalDelete").modal()
    $("#mmtrend_group_mode").val(mode);
    $("#mmtrend_group_b").val(id);
}
function doDeleteMmname(){
    var ids=[];
    var id="";
    var mode = $("#mmtrend_group_mode").val();
    if(mode=='deleteAll') {

        var length = document.getElementsByName('checkbox[]').length;
        for (var i = 0; i < length; i++) {
            var cbx = document.getElementsByName('checkbox[]')[i];
            if (cbx.checked) {
                ids.push(cbx.value)
            }
        }
        if (ids.length == 0) {
            $("#myModalDelete").modal('hide')
            return false;
        }
    }else{
        id=$("#mmtrend_group_b").val();
    }
    var obj={
        "mode":mode,
        "ZZ":id,
        "ids":ids
    }

    $.ajax({
        url: "/ajax/mmname/delete",
        method: "DELETE",
        data: obj
    }).done(function(data, status, xhr) {
        location.reload()
        $("#myModalDelete").modal('hide')
    });
}
// end delete mmtrend

// start display mmname for edit or save
function  displayMmname(mode,id){
    $("#trend_name_element").removeClass("has-error");
    $("#mmname_a").attr("placeholder", "ชื่อ Trend");
    $("#mmname_mode").val(mode)
    $("#mmname_zz").val(id)
    $("#mmname_a").val("");
    var obj={
        ZZ:id
    }
    $.ajax({
        url: "/ajax/mmname/get",
        method: "GET",
        data: obj
    }).done(function(data, status, xhr) {
        console.log(data);
        var mmnamesM = jQuery.parseJSON(data.mmnameM);
        var mmtrend_groups=jQuery.parseJSON(data.mmtrend_groups);
        // alert(mmtrend_groups)
        var mmname_tilte="แก้ใข";
        if(mode=='add')
            mmname_tilte="เพิ่ม";
        $("#button_mode_section").html(mmname_tilte);
        var mmname_b="";
        if(mmnamesM.length>0){
            $("#mmname_a").val(mmnamesM[0].A);
            mmname_b=mmnamesM[0].B;
        }

        $("#mmname_tilte_section").html(mmname_tilte+"  Trend");
        var group_str="<select id=\"mmtrend_group_select\" name=\"mmtrend_group_select\"  class=\"form-control m-b\">";
        if(mmtrend_groups!=null && mmtrend_groups.length>0){
            for(var i=0;i<mmtrend_groups.length;i++){
                var str_selected="";
                if(mmname_b==mmtrend_groups[i].B)
                    str_selected=" selected=\"selected\" ";
                group_str=group_str+" <option "+str_selected+" value=\""+mmtrend_groups[i].B+"\">"+mmtrend_groups[i].group_name+"</option>";
            }
        }
        group_str=group_str+"</select>";

        $("#mmtrend_group_select_element").html(group_str)
        $("#myModal2").modal()
    });
}
function  doActionMmname(){
    $("#trend_name_element").removeClass("has-error");
    $("#mmname_a").attr("placeholder", "ชื่อ Trend");
    var mmname_b=$("#mmtrend_group_select").val();
    //alert(mmname_b)
    var mode=$("#mmname_mode").val();
    var mmname_a=$.trim($("#mmname_a").val());
    if(mmname_a.length==0){
        $("#trend_name_element").addClass("has-error");
        $("#mmname_a").attr("placeholder", "กรุณากรอก ชื่อ Trend ");
        return false;
    }
    var mmname_zz=$("#mmname_zz").val();
    var obj={
        "A":mmname_a,
        "B":mmname_b,
        "ZZ":mmname_zz,
        "mode":mode
    }

    $.ajax({
        url: "/ajax/mmname/post",
        method: "POST",
        data: obj
    }).done(function(data, status, xhr) {
        console.log(data, status, xhr);
        var count = jQuery.parseJSON(data.count);
        var isExcessTrend = jQuery.parseJSON(data.isExcessTrend);
        // alert((isExcessTrend))
        if(isExcessTrend){
            $("#trend_name_element").addClass("has-error");
            //alert("คุณไม่สามารถเพิ่ม Trend ได้");

            var maxTrend = jQuery.parseJSON(data.maxTrend);
            $("#mmname_a").val("");
            $("#mmname_a").attr("placeholder", "คุณเพิ่ม Trend ได้มากที่สุด ["+maxTrend+"] Trends");
            return false;
        }else{
            var countInt=parseInt(count);
            if(countInt>0){
                $("#trend_name_element").addClass("has-error");
                $("#mmname_a").val("");
                $("#mmname_a").attr("placeholder", "ชื่อ Trend ["+mmname_a+"] ซ้ำ.");
            }else {
                location.reload()
                $("#myModal2").modal('hide')
            }
        }



    });
}

// end display mmtrend for edit or save

function  doActionMmtrend(){

    var mode=$("#mmtrend_mode").val();

    //var mmname_a=$("#mmname_a").val();
    var mmtrend_point_zz=$("#mmtrend_point_zz").val();
    var mmtrend_point_h=$("#mmtrend_point_h").val();
    var mmtrend_zz=$("#mmtrend_zz").val()
    var mmtrend_table_B= $("#mmtrend_table_B").val();
    //alert(mode+","+mmtrend_table_B);
    var mmtrend_point_a="";
    var length = document.getElementsByName('point_ids_input[]').length;
    for (var i = 0; i < length; i++) {
        var cbx = document.getElementsByName('point_ids_input[]')[i];
        if (cbx.checked) {
            mmtrend_point_a=cbx.value;
            break;
        }
    }
    var G0=$("#mmpoint_table_G0").val();
    var G1=$("#mmpoint_table_G1").val();
    var F=$("#mmpoint_table_F").val();
    var B=$("#mmtrend_table_B").val();
    //alert(mode+","+mmtrend_point_zz+","+mmtrend_point_a+","+mmtrend_zz)
    //$("#myModalAddPoint").modal('hide')

    var obj={
        "A":mmtrend_point_a,
        "ZZ":mmtrend_point_zz,
        "mode":mode,
        "G0":G0,
        "G1":G1,
        "F":F,
        "B":B,
        "G":mmtrend_zz
    }
    $.ajax({
        url: "/ajax/mmtrend/post",
        method: "POST",
        data: obj
    }).done(function(data, status, xhr) {
        // location.reload()
        showmmtrend(mmtrend_zz)
        // displayMmtrend('edit',mmtrend_point_zz,mmtrend_point_a);
        $("#myModalAddPoint").modal('hide')
    });

}
function setMaxMinValue(max,min,unit){
    $("#mmpoint_table_G0").val(max);
    $("#mmpoint_table_G1").val(min);
    $("#mmpoint_table_F").val(unit);
}
function displayLastObject(lastObject){
    //dataJson[dataJson.length-1]
    //console.log(dataJsonG)
    // var lastObject=dataJsonG[dataJsonG.length-1];
    //console.log(lastObject);
    //console.log(dataJsonG);
    for (var x in lastObject) {
        if (x != 'EvTime') {
            if(lastObject[x]!=null){
                var evtime=lastObject['EvTime'];
                //$("#planPoint-" + x).html(parseFloat(lastObject[x]).toFixed(2));
                if(dataJsonG[evtime][x] != null)
                    $("#planPoint-" + x).html(parseFloat(dataJsonG[evtime][x]).toFixed(2));
                else
                    $("#planPoint-" + x).html("0");
                console.log(x);
                console.log(dataJsonG[evtime][x]);
            }

        }
    }
    //$("#planPoint-"+series.field).html(parseFloat(value).toFixed(2));
}
function showTimeLabel(category,scaleTime){

    if(scaleTime=='hour'){
        var dateTimeArray=category.split(" ");
        var dateArray=dateTimeArray[0].split("/");

        var dateYear=dateArray[2];
        var dateMonth=parseInt(dateArray[1])-1;
        var dateday=dateArray[0];

        var dateTimeHis=dateTimeArray[1];
        //return dateday+" "+monthNameTh[parseInt(dateMonth)]+" "+(parseInt(dateYear)+543)+" เวลา "+dateTimeHis+":00:00";
        return dateday+" "+monthNameTh[parseInt(dateMonth)]+" "+dateYear+" เวลา "+dateTimeHis+":00:00";
    }
    else if (scaleTime == 'month') {
        var dateArray=category.split("/");// dateTimeArray[0].split("/");

        var dateYear=dateArray[1];
        var dateMonth=parseInt(dateArray[0])-1;
        return monthNameTh[parseInt(dateMonth)]+" "+dateYear;
    }else if (scaleTime == 'day') {
        var dateArray=category.split("/");// dateTimeArray[0].split("/");

        var dateYear=dateArray[2];
        var dateMonth=parseInt(dateArray[1])-1;
         var dateday=dateArray[0];
        return dateday+" "+monthNameTh[parseInt(dateMonth)]+" "+dateYear;
    }else if (scaleTime == 'minute') {
        // 06/07/2016 19:17:00
        var dateTimeArray=category.split(" ");
        var dateArray=dateTimeArray[0].split("/");

        var dateYear=dateArray[2];
        var dateMonth=parseInt(dateArray[1])-1;
        var dateday=dateArray[0];

        var dateTimeHis=dateTimeArray[1];
        return dateday+" "+monthNameTh[parseInt(dateMonth)]+" "+dateYear+" เวลา "+dateTimeHis;
        /*
        var startTime = $("#oneTime").val();
        // var endTime = $("#endTime").val();
        var dateArray = startTime.split("/");
        var dateYear=dateArray[2];
        var dateMonth=parseInt(dateArray[1])-1;
         var dateday=dateArray[0];

        //    var dateTimeHis=dateTimeArray[1];
        return dateday+" "+monthNameTh[parseInt(dateMonth)]+" "+dateYear+" เวลา "+category+"";

        // return monthNameTh[parseInt(dateMonth)]+" "+dateYear;
        */
    }else if (scaleTime == 'second') {
        // 06/07/2016 19:17:00
        var dateTimeArray=category.split(" ");
        var dateArray=dateTimeArray[0].split("/");

        var dateYear=dateArray[2];
        var dateMonth=parseInt(dateArray[1])-1;
        var dateday=dateArray[0];

        var dateTimeHis=dateTimeArray[1];
        return dateday+" "+monthNameTh[parseInt(dateMonth)]+" "+dateYear+" เวลา "+dateTimeHis;
        /*
        var startTime = $("#oneTime").val();
        var dateArray = startTime.split("/");
        var dateYear=dateArray[2];
        var dateMonth=parseInt(dateArray[1])-1;
        var dateday=dateArray[0];
        return dateday+" "+monthNameTh[parseInt(dateMonth)]+" "+dateYear+" เวลา "+category+"";
        */
    }
}
function templateFormat2(dataSource,category,series,value)
{

    // console.log(dataItem)
    console.log(dataJsonG[dataSource.category][series.field])
    var scaleTime=$("#scaleTime").val();
    console.log("dataSource["+dataSource.category+"],category["+category+"],series["+series.field+","+series.name+"],value["+value+"]")
    //$("#valuePoint-"+series.field).html(parseFloat(value).toFixed(2));
    if(dataJsonG[dataSource.category][series.field]!=null)
        $("#valuePoint-"+series.field).html(parseFloat(dataJsonG[dataSource.category][series.field]).toFixed(2));
    else
        $("#valuePoint-"+series.field).html("0");

    $("#dateInDataDisplay").html(showTimeLabel(category,scaleTime));


  //  $("#dateTimeInDataDisplayHour-"+paramTrendID+"").html(convertDateTh(lastObject['EvTime'])+" เวลา "+timeHIS[1]+" น.");
 // alert(category+","+series+","+value)
}
function setDataJsonG(dataJson) {

   // dataJsonG=dataJson;
   // console.log("test DataJsonG")
    //console.log(dataJsonG.length);
    //console.log(dataJsonG[0]);
   // var results = jQuery.parseJSON(dataJsonG);
    // console.log(results)
}
function  createChart(dataJson,series,paramStep,max_value,min_value) {
    setDataJsonG(dataJson);
    //console.log(dataJsonG)
    // center no-repeat url('http://localhost:8000/images/logo-egat.png')
    var majorUnit=parseInt((parseInt(max_value)-parseInt(min_value))/10);
    // alert(majorUnit)
    $("#chart").kendoChart({
        transitions: false,
        //renderAs: "canvas",
      //  autoBind: false,
        chartArea: {
            background:""
        },
        dataSource: {
             data: dataJson
        },
        title: {
            text: "Spain electricity production (GWh)",
            visible: false
        },
        legend: {
            position: "top",
            visible: false
        },
        seriesDefaults: {
            type: "line",
            style: "smooth",
            width:"2",
            missingValues: "gap",
          //  stack: true,
            markers: {
                visible: false
            }
        },
        series:series,
        categoryAxis: {
            field: "EvTime",
            labels: {
                rotation: -45,
                step: paramStep ,
            },
            crosshair: {
                visible:true
            },
            majorGridLines: {
                visible: false
            },
        },
        valueAxis: {
            type: "log",
            labels: {
                format: "N0"
            },
            minorGridLines: {
                //visible:true,
                visible:false,
               // color: "#2243ee"
            },

            line: {
                visible: false

            },
            axisCrossingValue: -10,
           visible: false,
            /* */
            max:100,
            min:0,
            majorUnit: 10
           /*
            max:max_value,

            majorUnit: majorUnit,
            min:min_value,
            */
           // color: "red",
            // visible: false
        },

        tooltip: {
            visible: true,
            shared: true,
            //format: "N0"
            //template: "#= series.name #: #= value #"
            template: "#= templateFormat2(data,category,series,value) #"
        },
        pannable: true,
        pannable: {
            lock: "y"
        }
    });

    displayLastObject(dataJson[dataJson.length-1]);
}
function createChart2(dataJson,series) {
  //alert(series.length)
    /*
    var series=[{
        field: "nuclear",
        name: "Nuclear"
    }, {
        field: "hydro",
        name: "Hydro"
    }, {
        field: "wind",
        name: "Wind"
    }];
    var dataJson=[
        {
            "country": "Spain",
            "EvTime": "2008",
            "unit": "GWh",
            "solar": 2578,
            "hydro": 26112,
            "wind": 32203,
            "nuclear": 58973
        },
        {
            "country": "Spain",
            "EvTime": "2007",
            "unit": "GWh",
            "solar": 508,
            "hydro": 30522,
            "wind": 27568,
            "nuclear": 55103
        },
        {
            "country": "Spain",
            "EvTime": "2006",
            "unit": "GWh",
            "solar": 119,
            "hydro": 29831,
            "wind": 23297,
            "nuclear": 60126
        },
        {
            "country": "Spain",
            "EvTime": "2005",
            "unit": "GWh",
            "solar": 41,
            "hydro": 23025,
            "wind": 21176,
            "nuclear": 57539
        },
        {
            "country": "Spain",
            "EvTime": "2004",
            "unit": "GWh",
            "solar": 56,
            "hydro": 34439,
            "wind": 15700,
            "nuclear": 63606
        },
        {
            "country": "Spain",
            "EvTime": "2003",
            "unit": "GWh",
            "solar": 41,
            "hydro": 43897,
            "wind": 12075,
            "nuclear": 61875
        },
        {
            "country": "Spain",
            "EvTime": "2002",
            "unit": "GWh",
            "solar": 30,
            "hydro": 26270,
            "wind": 9342,
            "nuclear": 63016
        },
        {
            "country": "Spain",
            "EvTime": "2001",
            "unit": "GWh",
            "solar": 24,
            "hydro": 43864,
            "wind": 6759,
            "nuclear": 63708
        },
        {
            "country": "Spain",
            "EvTime": "2000",
            "unit": "GWh",
            "solar": 18,
            "hydro": 31807,
            "wind": 4727,
            "nuclear": 62206
        }
    ];
    */
    $("#chart").kendoChart({
        dataSource: {
            data: dataJson
        },
        title: {
            text: "Spain electricity production (GWh)",
            visible: false
        },
        legend: {
            position: "top",
            visible:true
        },
        seriesDefaults: {
            type: "line",
            style: "smooth",
            width:"2",
            missingValues: "gap",
            stack: true,
            markers: {
                visible: false
            },
            // dashType:"solid"
        },
        series: series,

        categoryAxis: {
            field: "EvTime",
            labels: {
                rotation: -45,
                visible: true,
                //  step: 10 ,
                /*
                 step: paramStep ,
                 font: labelFontSize,
                 */

            },
            crosshair: {
                visible: true
            },
            majorGridLines: {
                visible: true,
               // step: 20
            }
        },
        valueAxis: {
            labels: {
                format: "N0"
            },
            visible: true,
            /*
            max:140000,
            min:0,
            majorUnit: 10000
            */
            //visible: false,
            /*
            max:100,
            min:0,
            majorUnit: 10
            */
        },

        tooltip: {
            visible: true,
            template: "#= templateFormat2(category,series.name,value) #",
            //category,series.name,value
           //template: "#= series.name #: #= value #",
            shared: true
        } // ,



        /*
        pannable: true,
        pannable: {
            lock: "y"
        }
        */
    });
}
function displayPointLeftArea(results){
    var str="";

    for (var i = 0; i < results.length; i++) {
        var dataTable=" <table class=''> "+
        " <tr> "+
        "  <td colspan='2'> "+
        "   <input type='checkbox' name='pointEvent' class='pointEvent i-checks' id='event-U04D1-3195' value='event'>&nbsp;Event "+
        " &nbsp; <input type='checkbox' class='pointEvent i-checks' name='pointEvent' id='action-U04D1-3195' value='action'>&nbsp;Action "+
        " &nbsp;<input type='checkbox' class='pointEvent i-checks' name='pointEvent' id='vpser-U04D1-3195' value='vpser'>&nbsp;VPSER "+
        " </td> "+
        " </tr> "+
        " <tr class='btnArea' > "+
        "   <td colspan='2' > "+
        "  <div style='padding:5px;'> "+
        "  <button class='btn btn-primary btn-xs  btnOkSetingPoint' id='psOk-U04D1' type='button'> "+
        "    OK "+
        "    </button> "+
        "    <button class='btn btn-white btn-xs  btnCancelSetingPoint' id='psCancel-U04D1' type='button'> "+
        "    Cancel "+
        "    </button> "+
        "     </div> "+
        "    </td> "+
        "    </tr> "+
        "    </table> ";
        var unit=(parseInt(results[i]['B'])>9?("U"+results[i]['B']):("U0"+results[i]['B']));
        str=str+"<li id='group_item_"+i+"'  style='' class=\"list-group-item \"> "+
            " <div class=\"row\"> "+
        " <div class=\"col-md-10 col-sm-10 col-xs-10 \"> "+
        " <div class=\"row \"> "+
        " <div class=\"col-md-12  col-sm-12 col-xs-12\" id=\"btnPoint-0\"> "+
        " <div id=\"labelTitle\"> "+
         /*
            "  <button id=\"U04D1\" type=\"button\" style=\"background: "+results[i]['dataColor']+";color:white;\" class=\"btn  btn-xs btnSetingPoint btnSetingPoint-1\" data-container=\"body\" data-toggle=\"popover\" data-placement=\"bottom\" title=\"\" data-html=\"true\" data-content=\" "+
        // dataTable+
                "  \" "+
        "  data-original-title=\"Event Seting\"> "+
            " <i class=\"fa fa-gear\"></i> "+
        "     </button> "+
                */
        "     <span style=\"color:"+results[i]['dataColor']+"\"> "+
        "     <span class=\"clickHideShowPoint showPoint\" id=\"showPoint-U04D1-3195\"> "+
        "     <div class=\"pointName\" id='pointName_"+i+"' onclick='showHideLegend("+i+")'> "+
        "     ["+unit+"] "+results[i]['C']+"                            </div> "+
        "     <div class=\"pointName\" id='tagName_"+i+"' style=\"display: none;\" onclick='showHideLegend("+i+")'> "+
        "     ["+unit+"] "+results[i]['D']+"                            </div> "+
        " <div class=\"pointTag pointTag-3195\">,U04 40SF61C502</div> "+
        " <div class=\"pointId pointId-3195\">,U04D1</div> "+
        "  <div class=\"pointId2 pointId2-3195\">,D1-4-92547</div> "+
        "  </span> "+
        " </span> "+
        "  </div> "+
        " <div id=\"setEvent\"> "+
        "    </div> "+
        "     </div> "+
        "     </div> "+
        "     <div class=\"row\"> "+
        "    <div class=\"col-md-12 col-sm-12 col-xs-12\"> "+
        "    <span class=\"pull-left valuePoint\" id=\"valuePoint-key_"+results[i]['G'] + "_" + results[i]['ZZ']+"\" style=\"color:"+results[i]['dataColor']+"\">0</span> "+
        "    <span class=\"pull-right planPoint\" id=\"planPoint-key_"+results[i]['G'] + "_" + results[i]['ZZ']+"\" style=\"color:"+results[i]['dataColor']+"\">0</span> "+
        "    </div> "+
        "    </div> "+
        "    </div> "+
        "     <div class=\"col-md-2 fontSize10 col-sm-2 col-xs-2 col-padding0 unitWidth\"> "+
        "     <div class=\"row\"> "+
        "    <div class=\"col-md-12 col-sm-12 col-xs-12\">"+results[i]['E']+"</div> "+
        "    </div> "+
        "    <div class=\"row\"> "+
        "    <div class=\"col-md-12 col-sm-12 col-xs-12\">"+results[i]['F0']+"</div> "+
        "    </div> "+
        "    <div class=\"row\"> "+
        "    <div class=\"col-md-12 col-sm-12 col-xs-12\">"+results[i]['F1']+"</div> "+
        "    </div> "+
        "    </div> "+
        "   </div> "+
        "    </li> "+
        "    <div style=\"display: none;\"> "+
        "   <div class=\"paramEmbedArea\" id=\"paramEmbedArea-U04D1-3195\"> "+
        "    </div> "+
        "    </div>";
    }
   // alert(str)
    $("#listPointLeftArea").html(str);
}
function displayDateTitle(scaleTime,startTime,endTime,startTimeArray,endTimeArray) {
    //alert(formulas)
    //
    if (scaleTime == 'hour') {
        $("#scaleDateTimeArea").html("ข้อมูลวันที่ " + startTimeArray[0] + " " + monthNameTh[(parseInt(startTimeArray[1]) - 1)] + " " + startTimeArray[2] + "" +
        " - " + endTimeArray[0] + " " + monthNameTh[(parseInt(endTimeArray[1]) - 1)] + " " + endTimeArray[2] + "");
    }else if (scaleTime == 'month') {
        $("#scaleDateTimeArea").html("ข้อมูลเดือน " + monthNameTh[(parseInt(startTimeArray[1]) - 1)] + " " + startTimeArray[2] + "" +
            " - "+ monthNameTh[(parseInt(endTimeArray[1]) - 1)] + " " + endTimeArray[2] + "");
    }else if (scaleTime == 'day') {
        $("#scaleDateTimeArea").html("ข้อมูลวันที่ " + startTimeArray[0] + " " + monthNameTh[(parseInt(startTimeArray[1]) - 1)] + " " + startTimeArray[2] + "" +
            " - " + endTimeArray[0] + " " + monthNameTh[(parseInt(endTimeArray[1]) - 1)] + " " + endTimeArray[2] + "");
    }else if (scaleTime == 'minute') {
       // $("#scaleDateTimeArea").html("ข้อมูลวันที่ " + startTimeArray[0] + " " + monthNameTh[(parseInt(startTimeArray[1]) - 1)] + " " + startTimeArray[2] + "" +
        //    " - " + endTimeArray[0] + " " + monthNameTh[(parseInt(endTimeArray[1]) - 1)] + " " + endTimeArray[2] + "");
        $("#scaleDateTimeArea").html("ข้อมูลวันที่ " + endTimeArray[0] + " " + monthNameTh[(parseInt(endTimeArray[1]) - 1)] + " " + endTimeArray[2] + "");
    }else if (scaleTime == 'second') {
        /* $("#scaleDateTimeArea").html("ข้อมูลวันที่ " + startTimeArray[0] + " " + monthNameTh[(parseInt(startTimeArray[1]) - 1)] + " " + startTimeArray[2] + "" +
            " - " + endTimeArray[0] + " " + monthNameTh[(parseInt(endTimeArray[1]) - 1)] + " " + endTimeArray[2] + ""); */
         $("#scaleDateTimeArea").html("ข้อมูลวันที่ " + endTimeArray[0] + " " + monthNameTh[(parseInt(endTimeArray[1]) - 1)] + " " + endTimeArray[2] + "");
    }
}
function executeFormula(scaleTime,obj) {

}
function showGraph(downloadFile,downloadType) {
    var checkbox_inner_list=[];
    var length = document.getElementsByName('checkbox_inner[]').length;
    for (var i = 0; i < length; i++) {
        var cbx = document.getElementsByName('checkbox_inner[]')[i];
        if (cbx.checked) {
            checkbox_inner_list.push(cbx.value);
        }
    }
    if(checkbox_inner_list.length==0){
        alert("กรุณาเลือก Point");
        return false;
    }
    if(checkbox_inner_list.length>12){
        alert("Plot Graph ได้มากที่สุด 12 Point");
        return false;
    }
    // alert(checkbox_inner_list)
    var obj={
        trendId:checkbox_inner_list
    }
    $.ajax({
        url: "/ajax/graph/getFormula",
        method: "POST",
        data: obj
    }).done(function(data, status, xhr) {
        console.log(data);
        console.log(data["trends"]);
        var results = jQuery.parseJSON(data["trends"]);
        var max_value = jQuery.parseJSON(data["max_value"]);
        var min_value = jQuery.parseJSON(data["min_value"]);
      //  alert(max_value+","+min_value);
        var trends=results;
        //console.log(results.length);
        var formulas = [];
        var key = [];
        var series = [];
        displayPointLeftArea(results)
        for (var i = 0; i < results.length; i++) {
            formulas.push(results[i]["dataUnit"]);
            var field = "key_"+results[i]["G"] + "_" + results[i]["ZZ"] ;//+ "_" + results[i]["dataUnit"];
            var color = results[i]["dataColor"] ;
            // alert(color)
            var name = results[i]["C"] ;
            // results[i]["E"] ; unit
            // results[i]["D"] ; tag Name
            // results[i]["F0"] ; max
            // results[i]["F1"] ; min


            key.push(field);
            var sery = {
                field: field,
                name: name,
                color:color
            }
            series.push(sery);
        }
        var scaleTime=$("#scaleTime").val();
        var startTime ;
        var endTime ;
        var startTimeArray ;
        var endTimeArray ;


        var startTimeFormat="";
        var endTimeFormat="";
        var url_param="";


        var paramStep=12;
        if (scaleTime == 'hour') {
             startTime = $("#startHRTime").val();
             endTime = $("#endHRTime").val();
            if(startTime.length==0 || endTime.length==0){
                alert("กรุณาเลือกวันที่")
                return false;
            }
             startTimeArray = startTime.split("/");
             endTimeArray = endTime.split("/");
            paramStep=12;
            var startTimeFormat = startTimeArray[2] + "-" + startTimeArray[1] + "-" + startTimeArray[0] + " 00:00:00";
            var endTimeFormat = endTimeArray[2] + "-" + endTimeArray[1] + "-" + endTimeArray[0] + " 00:00:00";
        }else if( scaleTime == 'month' ){
             startTime = $("#startMonthTime").val();
             endTime = $("#endMonthTime").val();
            if(startTime.length==0 || endTime.length==0){
                alert("กรุณาเลือกเดือน")
                return false;
            }
             startTimeArray = startTime.split("/");
             endTimeArray = endTime.split("/");
             paramStep=1;
            var startTimeFormat = startTimeArray[2] + "-" + startTimeArray[1] + "-" + startTimeArray[0] + " 00:00:00";
            var endTimeFormat = endTimeArray[2] + "-" + endTimeArray[1] + "-" + endTimeArray[0] + " 00:00:00";
        }else if( scaleTime == 'day' ){
            startTime = $("#startTime").val();
            endTime = $("#endTime").val();
            if(startTime.length==0 || endTime.length==0){
                alert("กรุณาเลือกวันที่")
                return false;
            }
            startTimeArray = startTime.split("/");
            endTimeArray = endTime.split("/");
            paramStep=1;
            var startTimeFormat = startTimeArray[2] + "-" + startTimeArray[1] + "-" + startTimeArray[0] + " 00:00:00";
            var endTimeFormat = endTimeArray[2] + "-" + endTimeArray[1] + "-" + endTimeArray[0] + " 00:00:00";
        }else if( scaleTime == 'minute' ){
             endTime = $("#oneTime").val();
            if(endTime.length==0){
                alert("กรุณาเลือกวันที่")
                return false;
            }
             endTimeArray = endTime.split("/");

            var startTimeForDisplay=$("#startTimeForDisplay").val();
            var scaleTimeMinuteExpand=$("#scaleTimeMinuteExpand").val();
            var nowMoment=moment(endTime+" "+startTimeForDisplay+":00", "DD/MM/YYYY HH:mm:ss");
            nowMoment.subtract(parseInt(scaleTimeMinuteExpand), 'hours');
            nowMoment=nowMoment.set('minute', "00");
            paramStep=60;
            var startTimeFormat = nowMoment.format("YYYY-MM-DD HH:mm:ss");
            var endTimeFormat = endTimeArray[2] + "-" + endTimeArray[1] + "-" + endTimeArray[0] + " "+startTimeForDisplay+":00";
        }else if( scaleTime == 'second' ){
            endTime = $("#oneTime").val();
            if(endTime.length==0){
                alert("กรุณาเลือกวันที่")
                return false;
            }
            endTimeArray = endTime.split("/");

            var startTimeForDisplay=$("#startTimeForDisplay").val();
            var scaleTimeSecondExpand=$("#scaleTimeSecondExpand").val();
            var nowMoment=moment(endTime+" "+startTimeForDisplay+":00", "DD/MM/YYYY HH:mm:ss");
            nowMoment.subtract(parseInt(scaleTimeSecondExpand), 'minutes');
            nowMoment=nowMoment.set('second', "00");

            paramStep=20;
            var startTimeFormat = nowMoment.format("YYYY-MM-DD HH:mm:ss");
            var endTimeFormat = endTimeArray[2] + "-" + endTimeArray[1] + "-" + endTimeArray[0] + " "+startTimeForDisplay+":00";

        }
        displayDateTitle(scaleTime,startTime,endTime,startTimeArray,endTimeArray);
            // alert(scaleTime)
        // see testCallDataSec() in cFormCalculation.js
        var user_mmplant=$("#user_mmplant").val();
        var server=47;
        if(user_mmplant=='1'){
            server=47;
            url_param="http://10.249.91.96/trendSecond47/";
            //url_param="http://localhost/";
        }else if(user_mmplant=='2'){
            server=813;
            url_param="http://10.249.91.207/trendSecond813/";
        }else if(user_mmplant=='3'){
            server=813;
            url_param="http://10.249.91.207/trendSecond813/";
        }
       // alert("user_mmplant"+user_mmplant)
        //alert(trends)
        var obj = {
            key: key,
            startTime: startTimeFormat,//"2014-05-01 00:00:00",
            endTime: endTimeFormat,//"2014-05-07 00:00:00",
              //scaleType:"month", // 12
            // scaleType:"day", // 30
            scaleType:scaleTime,//"hour", // 30
            // scaleType: "minute", // 24 hr
            // minute, hour , day, month
            server: server,
            trendID: "88",
            url:url_param,
            trends:trends,
            downloadFile:downloadFile,
            downloadType:downloadType,
            formulas: formulas,

            // paramStep:1 // day , month
            // paramStep:12 // hour
            // paramStep:60 // minute
            paramStep:paramStep // hour


        };
        if(scaleTime=='second'){
            /*
            obj["startTime"]="2014-05-20 00:00:00";
            obj["endTime"]="2014-05-20 00:03:00";
            obj["formulas"]=["(U08D122+U08D122)*U08D123","U08D122+U08D123","U08D122","U08D123"],
            */
            /*
            var startTimeTmp=obj["startTime"].split(" ");
            var endTimeTmp=obj["endTime"].split(" ");
            obj["startTime"]="2014-05-20 "+startTimeTmp[1];
            obj["endTime"]="2014-05-20 "+endTimeTmp[1];
            obj["formulas"]=["(U08D122+U08D122)*U08D123","U08D122+U08D123","U08D122","U08D123"]
            */
            //"formulas":["(U08D122+U08D122)*U08D123","U08D122+U08D123+CONSTANT@XXXXXX"],
           // alert("startTime["+obj["startTime"]+"] , endTime["+obj["endTime"]+"]");
            $.ajax({
                url: "/ajax/secdata",
                method: "POST",
                data: obj
            }).done(function(data, status, xhr) {

                console.log(data);
                var results = jQuery.parseJSON(data);

                dataJsonG=results.myShowData;
                var dataJson = results.mydata;
                if(downloadFile){
                    var fileName=results.myfile["fileName"];
                    var fileType=results.myfile["fileType"];
                    //alert(results.myfile["fileType"])
                    var div = document.createElement("div");
                    document.body.appendChild(div);
                    var path="/ais/exportdata?filename="+fileName+"&filetype="+fileType;
                    var path="/exportData/2016_06_30/"+fileName+"."+fileType;
                    var path="http://localhost:3000/v1/downloadFile?filename="+fileName+"&filetype="+fileType;

                    var path="http://10.249.99.107:8080/steamtable/rest/downloadFile?filename="+fileName+"&filetype="+fileType;
                    div.innerHTML = "<iframe width='0' height='0' scrolling='no' frameborder='0' src='" + path + "'></iframe>";
                    // alert(results.length)
                    //var dataJson = results;
                    closepopover();
                }else{
                    // alert(results.length)
                    //  var dataJson = results;
                    createChart(dataJson, series,obj.paramStep,max_value,min_value)
                    $("#trendAndPointElement").hide('slow');
                    $("#graphElement").show('slow');
                    setTimeout( function(){
                        // Do something after 1 second
                        $("#chart").getKendoChart().redraw();
                    }  , 1000 );
                    changeNameShow("showbyPointName");
                }

                // console.log(data.dataWithTimes);
                //console.log(data);
                /*
                //var sources = jQuery.parseJSON(data.sources);
                //var dataWithTimes =data.dataWithTimes;
                var dataWithTimes = jQuery.parseJSON(data.dataWithTimes);

                //alert(trends)
                //console.log(trends);
                var data2={
                    "formula":dataWithTimes,
                    "trends": trends,
                    "downloadFile":downloadFile,
                    "downloadType":downloadType,
                    "trendID":"88"
                }
               // alert("---data2---");
                console.log(data2);
                $.ajax({
                    url: "/ajax/postFormula",
                    method: "POST",
                    data: data2
                }).done(function(data22, status, xhr) {

                    //console.log(data22);
                    var results = jQuery.parseJSON(data22);

                    dataJsonG=results.myShowData;
                    var dataJson = results.mydata;
                    if(downloadFile){
                        var fileName=results.myfile["fileName"];
                        var fileType=results.myfile["fileType"];
                        //alert(results.myfile["fileType"])
                        var div = document.createElement("div");
                        document.body.appendChild(div);
                        var path="/ais/exportdata?filename="+fileName+"&filetype="+fileType;
                        var path="/exportData/2016_06_30/"+fileName+"."+fileType;
                         var path="http://localhost:3000/v1/downloadFile?filename="+fileName+"&filetype="+fileType;

                         var path="http://10.249.99.107:8080/steamtable/rest/downloadFile?filename="+fileName+"&filetype="+fileType;
                        div.innerHTML = "<iframe width='0' height='0' scrolling='no' frameborder='0' src='" + path + "'></iframe>";
                        // alert(results.length)
                        //var dataJson = results;
                        closepopover();
                    }else{
                        // alert(results.length)
                      //  var dataJson = results;
                        createChart(dataJson, series,obj.paramStep,max_value,min_value)
                        $("#trendAndPointElement").hide('slow');
                        $("#graphElement").show('slow');
                        setTimeout( function(){
                            // Do something after 1 second
                            $("#chart").getKendoChart().redraw();
                        }  , 1000 );
                        changeNameShow("showbyPointName");
                    }

                });

                */

            });
        }else{
            /*
            if(scaleTime=='minute'){
                obj["startTime"]="2014-05-20 00:00:00";
                obj["endTime"]="2014-05-20 10:00:00"; // span time 4 hr
            }
            */
           // alert("startTime["+obj["startTime"]+"] , endTime["+obj["endTime"]+"]");
            $.ajax({
                url: "/ajax/executeCalculation",
                method: "POST",
                data: obj
            }).done(function (data, status, xhr) {
                console.log(data);
                var results = jQuery.parseJSON(data);

                // alert(results.length)
                var dataJson = results.mydata;
                var trends_scale=results.trends_scale;
                dataJsonG=results.myShowData;
                // alert(trends_scale['key_3078_88272']['max_value'])
                if(downloadFile){
                    var fileName=results.myfile["fileName"];
                    var fileType=results.myfile["fileType"];
                    //alert(results.myfile["fileType"])
                    var div = document.createElement("div");
                    document.body.appendChild(div);
                    var path="/ais/exportdata?filename="+fileName+"&filetype="+fileType;
                    var path="/exportData/2016_06_30/"+fileName+"."+fileType;
                    var path="http://localhost:3000/v1/downloadFile?filename="+fileName+"&filetype="+fileType;
                    var path="http://10.249.99.107:8080/steamtable/rest/downloadFile?filename="+fileName+"&filetype="+fileType;
                    div.innerHTML = "<iframe width='0' height='0' scrolling='no' frameborder='0' src='" + path + "'></iframe>";
                    // alert(results.length)
                    //var dataJson = results;
                    closepopover();
                }else{
                    createChart(dataJson, series,obj.paramStep,max_value,min_value)
                    $("#trendAndPointElement").hide('slow');
                    $("#graphElement").show('slow');
                    setTimeout( function(){
                        // Do something after 1 second
                        $("#chart").getKendoChart().redraw();
                    }  , 1000 );
                    changeNameShow("showbyPointName");
                }

                // get reference to the chart widget
                // var chart = $("#chart").data("kendoChart");
                //  $("#chart").getKendoChart()._legendItemClick(0);
                //  $("#chart").getKendoChart().options.series[0].visible = false;
                //  $("#chart").getKendoChart().redraw();
// hide the second series

                // chart.options.series[0].visible = false

                // alert($("#trendChartArea-3195").width())
                // alert(Number($(window).width()))
// redraw the chart
                // alert(chart)
                // chart.redraw();
                // $("#chart").css( { width: Number($(window).width())+"px" });
                // chart.redraw();

                //console.log(results.formula[0].key);
            });
        }


    });
    // [{field: "U04D32",name: "U04D32",color: "#10c4b2"},{field: "U05D32",name: "U05D32",color: "#ff7663"}]






    $("#trendChart").kendoChart({
        theme: "Flat",
        height:600,
        //width:800,

        //theme: "MaterialBlack",
        //renderAs: "canvas",
        chartArea: {
            background: ""
        },
        /*
        dataSource: {
            data: dataJson
        },
    */
        title: {
            // text: "Spain electricity production (GWh)"
        },
        legend: {
            position: "bottom",
            visible: false
        },

        seriesDefaults: {
            type: "line",
            style: "smooth",
            width:"2",
            missingValues: "gap",
            stack: true,
            markers: {
                visible: false
            },
            // dashType:"solid"
        },
        //series:
        series: [{
            name: "Series 1",
            type: "column",
            data: [200, 450, 300, 125, 100]
        }, {
            name: "Series 2",
            type: "column",
            data: [210, null, 200, 100, null]
        }],
        categoryAxis: {
            categories: ["Mon", "Tue", "Wed", "Thu", "Fri"]
        },
        /*
        series:seriesData,
        categoryAxis: {
            field: "EvTime",
            labels: {
                rotation: -45,
                visible: true,
                step: paramStep ,
                font: labelFontSize,

            },
            crosshair: {
                visible: true
            },
            majorGridLines: {
                visible: true,
                step: 20
            }
        },

        valueAxis: {
            labels: {
                format: "N0"
            },
            visible: false,
            max:1000,
            min:0,
            majorUnit: 100
        },

        tooltip: {
            visible: true,
            template: "#= templateFormat(category,series.name,value) #",
            //category,series.name,value
            //template: "#= series.name #: #= value #",
            shared: true
        },

        zoomable: {
            mousewheel: {
                lock: "y"
            },
            selection: {
                lock: "y"
            }
        },

        pannable: true,
        pannable: {
            lock: "y"
        }
         */
    });
}
function closeGraph() {
    $("#trendAndPointElement").show('slow');
    $("#graphElement").hide('slow');
}
function redrawChart() {
    //$("#chart").getKendoChart()._legendItemClick(0);
    // $("#chart").getKendoChart().options.series[0].visible = false;
    $("#chart").getKendoChart().redraw();
}
function showHideLegend(pointIndex) {
   // alert('clicked '+pointIndex)
    $("#chart").getKendoChart()._legendItemClick(pointIndex);
   // alert($("#group_item_"+pointIndex).attr('style'))
    var oldStyle=$("#group_item_"+pointIndex).attr('style');
    if(oldStyle.length>0){
        $("#group_item_"+pointIndex).attr('style',"");
    }else{
        $("#group_item_"+pointIndex).attr('style',"opacity:0.2;background-color:gray");
    }
    // $("#chart").getKendoChart().options.series[0].visible = false;
    $("#chart").getKendoChart().redraw();
}
function changeNameShow(obj) {
    // alert(obj)
    $("#showByValue").val(obj)
    if($("#"+obj).hasClass( "checked" )){
       // $("#showByValue").val(obj)
    }else{
        $("#"+obj).addClass( "checked" );
        if(obj=='showbyPointName'){
            $("#showbyTagName").removeClass( "checked" );
        }else{
            $("#showbyPointName").removeClass( "checked" );
        }
        // addClass
    }
    var nameCheck="pointName_";
    var nameHide="tagName_";
    if(obj=='showbyTagName') {
        nameCheck = "tagName_";
        nameHide="pointName_";
    }
    $( 'div[id^="'+nameHide+'"]' ).hide();
    $( 'div[id^="'+nameCheck+'"]' ).each(function( index ) {
        $( this ).show();
        console.log( index + ": " + $( this ).html() );
    });
    // tagName_
    // $("#"+obj).attr("class","iradio_square-green")
   // $("#td_id").attr('class', 'newClass');
 //class="iradio_square-green checked"
}
function refreshChart() {
    /*
     var now=moment();
     var nowFr=now.format("DD/MM/YYYY HH:mm:ss");
    //moment().add(4, 'h');
    // moment().subtract(7, 'days');
    now.add(4, 'h');
    alert(now)

    var a = moment();
    var b = moment().add(4, 'h');;
    var diffDate=a.diff(b, 'hours') // years, months, weeks, days, hours, minutes, and seconds
    alert(diffDate)

    var startTime= $("#startTime").val();
    var endTime= $("#endTime").val();
    var timeHis= $("#timeHis").val();
    */
  //  alert(startTime+","+endTime+","+timeHis)
 // var chart = $("#chart").data("kendoChart");
    // console.log(chart.options.series[0]);
  //  var color=chart.options.series[0].color;//.data[0].color;
   // alert("color"+color)
    showGraph(false,'');
}
function displayElementScaleTime(obj) {
    var scaleTime=obj.value;
   // alert(scaleTime)
    if(scaleTime=='month'){
        // hide
        $("#date_day").hide();
        $("#date_hr").hide();

        $("#datepicker_oneselect").hide();

        $("#scaleTimeMenuRightArea").hide();
        $("#scaleTimeMenuLeftArea").hide();
        $("#timeExpend").hide();
        // show
        $("#date_month").show();
        $("#datepicker_twoselect").show();

    }else if(scaleTime=='day'){
        // hide
        $("#date_month").hide();
        $("#date_hr").hide();

        $("#datepicker_oneselect").hide();

        $("#scaleTimeMenuRightArea").hide();
        $("#scaleTimeMenuLeftArea").hide();
        $("#timeExpend").hide();
        // show
        $("#date_day").show();
        $("#datepicker_twoselect").show();

    }else if(scaleTime=='hour'){
        // hide
        $("#date_month").hide();
        $("#date_day").hide();

        $("#datepicker_oneselect").hide();

        $("#scaleTimeMenuRightArea").hide();
        $("#scaleTimeMenuLeftArea").hide();
        $("#timeExpend").hide();
        $("#showHis").hide();
        // show
        $("#date_hr").show();
        $("#datepicker_twoselect").show();

        // $("#showHis").show();

    }else if(scaleTime=='minute'){

        var nowStartTimeForDisplay=moment().format("HH:00");
        $("#startTimeForDisplay").val(nowStartTimeForDisplay)
        // hide
        $("#datepicker_twoselect").hide();

        $("#scaleTimeMenuLeftArea").html($("#scaleTimeMinuteExpand").val()+" Hour");
        $("#scaleTimeMenuRightArea").show();
        $("#scaleTimeMenuLeftArea").show();
        $("#timeExpend").show();
        // show
        $("#datepicker_oneselect").show();

    }else if(scaleTime=='second'){
        var nowStartTimeForDisplay=moment().format("HH:mm");
        $("#startTimeForDisplay").val(nowStartTimeForDisplay)
        // hide
        $("#datepicker_twoselect").hide();

        $("#scaleTimeMenuLeftArea").html($("#scaleTimeSecondExpand").val()+" Min");
        $("#scaleTimeMenuRightArea").show();
        $("#scaleTimeMenuLeftArea").show();
        $("#timeExpend").show();
        // show
        $("#datepicker_oneselect").show();

    }else{
        $("#scaleTimeMenuRightArea").hide();
        $("#scaleTimeMenuLeftArea").hide();
        $("#timeExpend").hide();
        $("#showHis").hide();
    }

}
function changeStartTime(changeMode){
    // changeMode = plus,minus , max ,min
    //alert(changeMode)
    var scaleTime=$("#scaleTime").val();
    var startTimeForDisplay=$("#startTimeForDisplay").val();
    if(scaleTime=='minute'){
        var oneTime=$("#oneTime").val();
        var oneTimeArray=oneTime.split("/");
        var startTimeForDisplayArray=startTimeForDisplay.split(":");
        var nowMoment=moment(oneTime+" "+startTimeForDisplay, "DD/MM/YYYY HH:mm");
       // nowMoment.set('minute', startTimeForDisplayArray[0]);
        nowMoment=nowMoment.set('minute', "00");
        if(changeMode=='plus') {
            if(parseInt(startTimeForDisplayArray[0])<23)
                nowMoment.add(1, 'hours');
        }
        else if(changeMode=='minus') {
            if(parseInt(startTimeForDisplayArray[0])>0)
                nowMoment.subtract(1, 'hours');
            //moment().add(4, 'h');
            // moment().subtract(7, 'days');
        }else if(changeMode=='max') {
            nowMoment=nowMoment.set('hour', "23");
        }else if(changeMode=='min') {
            nowMoment=nowMoment.set('hour', "00");
        }
        console.log(nowMoment)
        $("#startTimeForDisplay").val(nowMoment.format("HH:mm"));
    }else if(scaleTime=='second'){
        var oneTime=$("#oneTime").val();
        var oneTimeArray=oneTime.split("/");
        var startTimeForDisplayArray=startTimeForDisplay.split(":");
        var nowMoment=moment(oneTime+" "+startTimeForDisplay, "DD/MM/YYYY HH:mm");
        // nowMoment.set('minute', startTimeForDisplayArray[0]);
        //nowMoment.set('second', "00");
        if(changeMode=='plus') {
            if(!(parseInt(startTimeForDisplayArray[0])==23 && parseInt(startTimeForDisplayArray[1])==59))
                nowMoment.add(1, 'minutes');
        }
        else if(changeMode=='minus') {
            if(!(parseInt(startTimeForDisplayArray[0])==0 &&  parseInt(startTimeForDisplayArray[1])==0))
                nowMoment.subtract(1, 'minutes');
            //moment().add(4, 'h');
            // moment().subtract(7, 'days');
        }else if(changeMode=='max') {
            nowMoment=nowMoment.set('hour', "23");
            nowMoment=nowMoment.set('minute', "59");
        }else if(changeMode=='min') {
            nowMoment=nowMoment.set('hour', "00");
            nowMoment=nowMoment.set('minute', "00");
        }
        console.log(nowMoment)
        $("#startTimeForDisplay").val(nowMoment.format("HH:mm"));
    }
    showGraph(false,'');
}
function changeSpanTime(changeMode){
    // changeMode = plus , minus
   // alert(changeMode)
    var scaleTimeExpand="";
    var scaleTime=$("#scaleTime").val();
    if(scaleTime=='minute'){ // max 12 hr [1,2,3,4,6,8,10,12] 24
         scaleTimeExpand=$("#scaleTimeMinuteExpand").val();
        if(changeMode=='plus') {
            if(parseInt(scaleTimeExpand)<24) {
                var offset=1;
                if(parseInt(scaleTimeExpand)>3)
                    offset=2;
                scaleTimeExpand = parseInt(scaleTimeExpand) + offset;
                $("#scaleTimeMinuteExpand").val(scaleTimeExpand);
            }
        }
        else if(changeMode=='minus') {
            if(parseInt(scaleTimeExpand)>1) {
                var offset=1;
                if(parseInt(scaleTimeExpand)>4)
                    offset=2;
                scaleTimeExpand = parseInt(scaleTimeExpand) - offset;
                $("#scaleTimeMinuteExpand").val(scaleTimeExpand);
            }
        }
        $("#scaleTimeMenuLeftArea").html(scaleTimeExpand+" Hour");
    }else if(scaleTime=='second') {
         scaleTimeExpand=$("#scaleTimeSecondExpand").val();
        if(changeMode=='plus') {
            if(parseInt(scaleTimeExpand)<10) {
                scaleTimeExpand = parseInt(scaleTimeExpand) + 1;
                $("#scaleTimeSecondExpand").val(scaleTimeExpand);
            }
        }
        else if(changeMode=='minus') {
            if(parseInt(scaleTimeExpand)>1) {
                scaleTimeExpand = parseInt(scaleTimeExpand) - 1;
                $("#scaleTimeSecondExpand").val(scaleTimeExpand);
            }
        }
        $("#scaleTimeMenuLeftArea").html(scaleTimeExpand+" Min");
    }
    showGraph(false,'');
}
function closepopover() {
    // $(".popover").hide();
    $("#downloadSettingArea").click();
  //  alert('xx')
}
function downloadDataFile() {
    var  downloadFile=true;
    var downloadType=$('input[name=download]:checked').val();
    if (typeof downloadType === "undefined") {
        alert('กรุณาเลือก file Type');
        return false;
    }else{
        // alert(downloadType)
        showGraph(downloadFile,downloadType);
    }

    //type='radio' name='download'
    // var  downloadType=ods;

}