<!DOCTYPE html>
<html>


<body>

 <div id='tooltip'class='tooltipClass'></div> 
    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            @yield('nevigation_slideMenu')
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            @yield('body')
                <div class="row border-bottom">@yield('navigation_Header')</div>
                <div class="content">@yield('content')</div>
                <div class="footer">@yield('footer')</div>
        </div>
    </div>

</body>
</html>

