<?php
/**
 * Created by PhpStorm.
 * User: imake
 * Date: 20/06/2016
 * Time: 14:28
 */

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Log;
use Session;
use Illuminate\Support\Facades\DB;
use \App\Utils\DBUtils;
use Illuminate\Support\Facades\Auth;
class GraphAjax extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getFormula(Request $request){
        /*
        $colorFlatTheme=["#10c4b2", "#ff7663", "#ffb74f", "#a2df53", "#10c4b2","#ff63a5","#1cc47b",
            "#10c4b2","#ff7663","#ffb74f","#a2df53","#1c9ec4"];
        */
        $colorFlatTheme=["#0008ff", "#e51717", "#06f236", "#00a5ff", "#fb00ff",
                         "#f9e700","#00ddff","#f5caff","#2cb484","#9381fb",
                        "#a2f3ff","#80ff00"];

        $COLOR_ATTR=["A","B","C","D","E","F","G","H","I","J","K","L"];
        $trendId_param = request('trendId');
        $sess_emp_id= Auth::user()->empId;
        Log::info($sess_emp_id);
        Log::info($trendId_param);
      //  $a="A";
       // DB::connection()
        $sql_color="select * from mmtrend_color_table where O='$sess_emp_id' ";
        $mmtrend_colorM = DB::connection()->select($sql_color);
            // ->where('mmtrend_color.O', '=', $sess_emp_id)->get();
        //Log::info($mmtrend_colorM[0]->$a);
        $trends = array();
        $max_value=0;
        $min_value=0;
        foreach ($trendId_param as $key => $trendId) {
            $trendDesignsM = DB::connection(DBUtils::getDBName())->table('mmtrend_table as mmtrend ')
                ->where('mmtrend.ZZ', '=', $trendId)->get();
            //Log::info($trendDesignsM);
            Log::info($key);
            if($key==0){
                $max_value=$trendDesignsM[0]->F0;
                $min_value=$trendDesignsM[0]->F1;
            }
            $dataColor="";
            if (!empty($mmtrend_colorM) && $key<12){
                if(!empty($mmtrend_colorM[0]->$COLOR_ATTR[$key]))
                    $dataColor = $mmtrend_colorM[0]->$COLOR_ATTR[$key];
                else
                    $dataColor = $colorFlatTheme[$key];
            }else{
                $dataColor = $colorFlatTheme[$key];
            }

            //find Max
            if(!empty($trendDesignsM[0]->F0) && $trendDesignsM[0]->F0>$max_value){
                $max_value=$trendDesignsM[0]->F0;
            }
            //find Min
            if(!empty($trendDesignsM[0]->F1) && $trendDesignsM[0]->F1<$min_value){
                $min_value=$trendDesignsM[0]->F1;
            }
            // get UNIT
            $dataUnit="";
            if($trendDesignsM[0]->H!=0){
                $unit="U";
                if($trendDesignsM[0]->B<10)
                    $unit="U0";
                $dataUnit=$unit.$trendDesignsM[0]->B."D".$trendDesignsM[0]->H;
            }else{ // Calculation
                $mmcalculationM = DB::connection(DBUtils::getDBName())->table('mmcalculation_table as mmcalculation ')
                    ->where('mmcalculation.A', '=', $trendDesignsM[0]->I)->get();
                $dataUnit=trim($mmcalculationM[0]->G);
            }
            $trendDesignsM[0]->dataColor = $dataColor;
            $trendDesignsM[0]->dataUnit = $dataUnit;

            array_push($trends,$trendDesignsM[0]);
        }

        $trendsJson=json_encode($trends);
        Log::info($trendsJson);
        return response()->json(['trends'=>$trendsJson,'max_value'=>$max_value,'min_value'=>$min_value]);

    }
    public function listMmTrend(Request $request){
        Log::info("Into listMmTrend ");
        //Log::info(request('xx')['yy']);
        $g_data=request('zz');
        $pageNo=request('pageNo');
        $pageSize=request('pageSize');
        $mmnameM = DB::connection(DBUtils::getDBName())->table('mmname_table')->where('ZZ', $g_data)->first();
        $trendDesignsM = DB::connection(DBUtils::getDBName())->table('mmtrend_table as mmtrend ')
            //  ->join('mmname_table as mmname ', 'mmtrend.G', '=', 'mmname.ZZ')
            //   ->join('orders', 'users.id', '=', 'orders.user_id')
            //   ->select('users.*', 'contacts.phone', 'orders.price')
            //  ->select('mmtrend.*', 'mmname.A')
            ->where('mmtrend.G', '=', $g_data)
            ->orderBy('mmtrend.A','ASC')
            ->skip(($pageNo-1)*$pageSize)->take($pageSize)->get();
        //->paginate(100);
        $count = DB::connection(DBUtils::getDBName())->table('mmtrend_table')->where('G','=',$g_data)->count();
        // $data   = array('value' => request('xx').' some data');
        // return response()->json(['name' => 'Abigail', 'state' => 'CA']);
        //return response()->json($trendDesignsM);
        //return response()->json(['trendDesignsM'=>$trendDesignsM->toJson(),
        return response()->json(['trendDesignsM'=>json_encode($trendDesignsM),
            'count'=>json_encode($count),
            // 'paging' => $trendDesignsM->render(),
            'mmnameM'=>json_encode($mmnameM)]);
    }
    public function listMmPoint(Request $request){
        Log::info("Into listMmPoint ");

    }
    public function displayGraph(Request $request){
        Log::info("Into displayGraph");
    }
}
